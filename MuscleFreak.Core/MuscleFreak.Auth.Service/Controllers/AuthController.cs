﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using MuscleFreak.Auth.Service.Entities;
using MuscleFreak.Auth.Service.Models;

namespace MuscleFreak.Auth.Service.Controllers
{
    [RoutePrefix("api")]
    public class AuthController : ApiController
    {
        [HttpPost]
        [Route("register")]
        public void Register([FromBody]UserModel userModel)
        {
            var authRepository = new MongoAuthContext();

            authRepository.Register(new User()
            {
                Email = userModel.Email,
                Password = userModel.Password,
                Username = userModel.Username
            });
        }
    }
}