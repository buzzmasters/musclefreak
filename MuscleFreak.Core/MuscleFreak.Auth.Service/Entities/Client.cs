﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson.Serialization.Attributes;

namespace MuscleFreak.Auth.Service.Entities
{
    public class Client
    {
        [BsonId]
        public string Id { get; set; }

        [BsonElement("secret")]
        public string Secret { get; set; }

        [BsonElement("name")]
        public string Name { get; set; }

        [BsonElement("isActive")]
        public bool IsActive { get; set; }

        [BsonElement("refreshTokenLifeTime")]
        public int RefreshTokenLifeTime { get; set; }

        [BsonElement("allowedOrigin")]
        public string AllowedOrigin { get; set; }
    }
}