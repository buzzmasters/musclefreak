﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MongoDB.Bson.Serialization.Attributes;

namespace MuscleFreak.Auth.Service.Entities
{
    public class User
    {
        [BsonId]
        public Guid Id { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("email")]
        public string Email { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("email_confirmed")]
        public string EmailConfirmed { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("username")]
        public string Username { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("password")]
        public string Password { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("roles")]
        public IEnumerable<string> Roles { get; set; }
    }
}