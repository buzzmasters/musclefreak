﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using MongoDB.Bson;
using MongoDB.Driver;
using MuscleFreak.Auth.Service.Entities;
using MuscleFreak.Auth.Service.Models;

namespace MuscleFreak.Auth.Service
{
    public class MongoAuthContext
    {
        private readonly string _userCollectionName = ConfigurationManager.AppSettings["user_collection_name"];
        private readonly string _clientCollectionName = ConfigurationManager.AppSettings["client_collection_name"];
        private readonly string _refreshTokenCollectionName = ConfigurationManager.AppSettings["refresh_token_collection_name"];

        private readonly IMongoDatabase _database;

        public IMongoCollection<User> Users { get; }
        public IMongoCollection<Client> Clients { get; }
        public IMongoCollection<RefreshToken> RefreshTokens { get; }

        public MongoAuthContext()
        {
            string mongoDbConnectionString = ConfigurationManager.ConnectionStrings["mongo_database"].ConnectionString;
            var client = new MongoClient(mongoDbConnectionString);
            var databaseName = MongoUrl.Create(mongoDbConnectionString).DatabaseName;
            _database = client.GetDatabase(databaseName);
            Users = _database.GetCollection<User>(_userCollectionName);
            Clients = _database.GetCollection<Client>(_clientCollectionName);
            RefreshTokens = _database.GetCollection<RefreshToken>(_refreshTokenCollectionName);
        }

        public void RemoveUserById(Guid id)
        {
            var filter = Builders<User>.Filter.Eq(u => u.Id, id);

            Users.DeleteMany(filter);
        }

        public void RemoveUserByUsername(string username)
        {
            var filter = Builders<User>.Filter.Eq(u => u.Username, username);

            Users.DeleteMany(filter);
        }

        public RegistrationStatus Register(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException(nameof(user));
            }

            if (String.IsNullOrEmpty(user.Username))
            {
                throw new ArgumentNullException(nameof(user.Username));
            }

            if (String.IsNullOrEmpty(user.Password))
            {
                throw new ArgumentNullException(nameof(user.Password));
            }

            var usernameFilter = Builders<User>.Filter.Regex(u => u.Username, new BsonRegularExpression("/^" + user.Username + "$/i"));

            if (Users.Find(usernameFilter).Limit(1).Any())
            {
                return RegistrationStatus.AlreadyExists;
            }

            user.Password = BCrypt.Net.BCrypt.HashPassword(user.Password, BCrypt.Net.BCrypt.GenerateSalt(12));

            Users.InsertOne(user);
            return RegistrationStatus.Success;
        }

        public User FindUser(string username, string password)
        {
            if (String.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException(nameof(username));
            }

            if (String.IsNullOrEmpty(password))
            {
                throw new ArgumentNullException(nameof(password));
            }

            var foundUser = FindUserByUsername(username);
            if (foundUser == null)
            {
                return null;
            }

            return BCrypt.Net.BCrypt.Verify(password, foundUser.Password) ? foundUser : null;
        }

        public User FindUserByUsername(string username)
        {
            if (String.IsNullOrEmpty(username))
            {
                throw new ArgumentNullException(nameof(username));
            }

            var usernameFilter = Builders<User>.Filter.Regex(u => u.Username, new BsonRegularExpression("/^" + username + "$/i"));

            return Users.Find(usernameFilter).Limit(1).FirstOrDefault();
        }

        public void DropUsers()
        {
            _database.DropCollection(_userCollectionName);
        }
    }
}