﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MuscleFreak.Notes.Common.Test
{
    [TestFixture]
    public class NotesRepositoryTests
    {
        private readonly NotesRepository _notesRepository = new NotesRepository();

        [SetUp]
        public void SetUp()
        {
            _notesRepository.DeleteAll();
            foreach (var note in PresetNotes)
            {
                _notesRepository.Add(note);
            }
        }

        [Category("ExternalDependent")]
        [Test]
        public async Task Should_Retrieve_Notes_For_User()
        {
            var notes = (await _notesRepository.GetNotes(new NoteSearchRequest()
            {
                Username = "user1"
            })).ToList();

            Assert.AreEqual(2, notes.Count);
            Assert.AreEqual(1f, notes[0].BicepsLeft);
            Assert.AreEqual(2f, notes[1].BicepsLeft);
        }

        [Category("ExternalDependent")]
        [Test]
        public async Task Should_Filter_Notes_By_Date()
        {
            var notes = (await _notesRepository.GetNotes(new NoteSearchRequest()
            {
                Username = "user1",
                DateFrom = new DateTime(2017, 5, 19),
                DateTo = new DateTime(2017, 5, 20)
            })).ToList();

            Assert.AreEqual(1, notes.Count);
            Assert.AreEqual(1f, notes[0].BicepsLeft);
        }

        [Category("ExternalDependent")]
        [Test]
        public void Should_Throw_If_No_User_Passed()
        {
            Assert.That(async () => await _notesRepository.GetNotes(new NoteSearchRequest()),
                Throws.ArgumentNullException);
        }

        private IEnumerable<Note> PresetNotes => new List<Note>()
        {
            new Note()
            {
                Username = "user1",
                Date = new DateTime(2017, 5, 20),
                Id = Guid.Parse("b306a983-e0e1-4856-9a52-c0c37aaaf585"),
                BicepsLeft = 1f,
                BicepsRight = 1f,
                Chest = 1f
            },
            new Note()
            {
                Username = "user1",
                Date = new DateTime(2017, 5, 21),
                Id = Guid.Parse("88aef9b1-eae7-4366-a4f4-4fd79edd5cd6"),
                BicepsLeft = 2f,
                BicepsRight = 2f,
                Chest = 2f
            },
            new Note()
            {
                Username = "user2",
                Date = new DateTime(2017, 5, 22),
                Id = Guid.Parse("e751c925-9d9d-49b6-a807-bf01e39e03df"),
                BicepsLeft = 3f,
                BicepsRight = 3f,
                Chest = 3f
            },
            new Note()
            {
                Username = "user3",
                Date = new DateTime(2017, 5, 23),
                Id = Guid.Parse("43742e0c-392a-4e7c-9a0a-b43beec2e05a"),
                BicepsLeft = 4f,
                BicepsRight = 4f,
                Chest = 4f
            }
        };
    }
}
