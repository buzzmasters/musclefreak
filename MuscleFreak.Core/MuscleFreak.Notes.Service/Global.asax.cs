﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Routing;
using AutoMapper;
using MuscleFreak.Notes.Common;
using MuscleFreak.Notes.Service.ViewModels;

namespace MuscleFreak.Notes.Service
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
            Mapper.Initialize(cfg => cfg.CreateMap<NoteViewModel, Note>());
        }
    }
}
