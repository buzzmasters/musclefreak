﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using AutoMapper;
using MuscleFreak.Notes.Common;
using MuscleFreak.Notes.Service.ViewModels;

namespace MuscleFreak.Notes.Service.Controllers
{
    [Authorize]
    [RoutePrefix("api/notes")]
    public class NotesController : ApiController
    {
        [HttpGet]
        [Route("")]
        public async Task<IEnumerable<Note>> GetNotes([FromUri]NoteSearchRequest searchRequest)
        {
            searchRequest.Username = User.Identity.Name;
            var notesRepository = new NotesRepository();
            return await notesRepository.GetNotes(searchRequest);
        }
        
        [HttpPost]
        [Route("")]
        public Note AddNote(NoteViewModel noteViewModel)
        {
            var note = Mapper.Map<Note>(noteViewModel);
            note.Username = User.Identity.Name;
            note.Date = DateTime.UtcNow;
            note.Id = Guid.NewGuid();
            var notesRepository = new NotesRepository();
            notesRepository.Add(note);

            return note;
        }
    }
}
