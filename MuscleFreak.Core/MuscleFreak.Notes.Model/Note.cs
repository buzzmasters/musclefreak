﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MuscleFreak.Notes.Model
{
    [Bson]
    public class Note
    {
        [JsonProperty("bicepsL")]
        public float BicepsLeft { get; set; }

        [JsonProperty("bicepsR")]
        public float BicepsRight { get; set; }

        [JsonProperty("chest")]
        public float Chest { get; set; }

        [JsonProperty("waist")]
        public float Waist { get; set; }

        /// <summary>
        /// Бедро
        /// </summary>
        [JsonProperty("hipL")]
        public float HipL { get; set; }

        [JsonProperty("hipR")]
        public float HipR { get; set; }

        /// <summary>
        /// Голень
        /// </summary>
        [JsonProperty("shinL")]
        public float ShinL { get; set; }

        [JsonProperty("shinR")]
        public float ShinR { get; set; }

        [JsonProperty("forearmL")]
        public float ForearmL { get; set; }

        [JsonProperty("forearmR")]
        public float ForearmR { get; set; }

        [JsonProperty("weight")]
        public float Weight { get; set; }
    }
}
