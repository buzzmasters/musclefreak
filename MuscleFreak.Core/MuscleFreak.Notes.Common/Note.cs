﻿using System;
using MongoDB.Bson.Serialization.Attributes;
using Newtonsoft.Json;

namespace MuscleFreak.Notes.Common
{
    [JsonObject]
    public class Note
    {
        [BsonId]
        [JsonProperty("id")]
        public Guid Id { get; set; }

        [BsonElement("username")]
        [JsonProperty("username")]
        public string Username { get; set; }

        [BsonElement("date")]
        [JsonProperty("date")]
        public DateTime? Date { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("bicepsL")]
        [JsonProperty("bicepsL")]
        public double? BicepsLeft { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("bicepsR")]
        [JsonProperty("bicepsR")]
        public double? BicepsRight { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("chest")]
        [JsonProperty("chest")]
        public double? Chest { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("waist")]
        [JsonProperty("waist")]
        public double? Waist { get; set; }

        /// <summary>
        /// Бедро
        /// </summary>
        [BsonIgnoreIfNull]
        [BsonElement("hipL")]
        [JsonProperty("hipL")]
        public double? HipL { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("hipR")]
        [JsonProperty("hipR")]
        public double? HipR { get; set; }

        /// <summary>
        /// Голень
        /// </summary>
        [BsonIgnoreIfNull]
        [BsonElement("shinL")]
        [JsonProperty("shinL")]
        public double? ShinL { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("shinR")]
        [JsonProperty("shinR")]
        public double? ShinR { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("forearmL")]
        [JsonProperty("forearmL")]
        public double? ForearmL { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("forearmR")]
        [JsonProperty("forearmR")]
        public double? ForearmR { get; set; }

        [BsonIgnoreIfNull]
        [BsonElement("weight")]
        [JsonProperty("weight")]
        public double? Weight { get; set; }
    }
}
