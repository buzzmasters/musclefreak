﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;

namespace MuscleFreak.Notes.Common
{
    public class NotesRepository
    {
        private readonly IMongoClient _client;
        private readonly IMongoDatabase _database;
        private readonly IMongoCollection<Note> _notesCollection;

        public NotesRepository()
        {
            string mongoDbConnectionString = ConfigurationManager.ConnectionStrings["mongo_database"].ConnectionString;
            _client = new MongoClient(mongoDbConnectionString);
            var databaseName = MongoUrl.Create(mongoDbConnectionString).DatabaseName;
            _database = _client.GetDatabase(databaseName);

            string notesCollectionName = ConfigurationManager.AppSettings["notes_collection_name"];
            _notesCollection = _database.GetCollection<Note>(notesCollectionName);
        }

        public async Task<IEnumerable<Note>> GetNotes(NoteSearchRequest searchRequest)
        {
            if (searchRequest == null)
            {
                throw new ArgumentNullException(nameof(searchRequest));
            }

            if (searchRequest.Username == null)
            {
                throw new ArgumentNullException(nameof(searchRequest.Username));
            }

            var filterBuilder = Builders<Note>.Filter;
            FilterDefinition<Note> filter = filterBuilder.Eq(note => note.Username, searchRequest.Username);
            if (searchRequest.DateFrom.HasValue)
            {
                filter &= filterBuilder.Gte(e => e.Date, searchRequest.DateFrom.Value);
            }
            if (searchRequest.DateTo.HasValue)
            {
                filter &= filterBuilder.Lte(e => e.Date, searchRequest.DateTo.Value);
            }
            var options = new FindOptions<Note>()
            {
                Sort = Builders<Note>.Sort.Ascending(e=>e.Date)
            };
            IAsyncCursor<Note> result = null;
            try
            {
                result = await _notesCollection.FindAsync(filter, options);
            }
            catch (Exception e)
            {
                Console.WriteLine();
            }
            return result.ToList();
        }

        public async Task<Note> GetByIdAsync(Guid id)
        {
            var filter = Builders<Note>.Filter.Eq(e => e.Id, id);
            var findResult = await _notesCollection.FindAsync(filter);
            return findResult.FirstOrDefault();
        }

        public void Add(Note note)
        {
            _notesCollection.InsertOne(note);
        }

        public void Delete(Guid id)
        {
            var filter = Builders<Note>.Filter.Eq(note => note.Id, id);
            _notesCollection.DeleteOne(filter);
        }

        public void DeleteAll()
        {
            var filter = Builders<Note>.Filter.Empty;
            _notesCollection.DeleteMany(filter);
        }
    }
}
