﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MuscleFreak.Notes.Common
{
    [JsonObject]
    public class NoteSearchRequest
    {
        [JsonProperty("dateFrom")]
        public DateTime? DateFrom { get; set; }
        [JsonProperty("dateTo")]
        public DateTime? DateTo { get; set; }
        [JsonProperty("username")]
        public string Username { get; set; }
        [JsonProperty("pageSize")]
        public int PageSize { get; set; }
        [JsonProperty("pageNumber")]
        public int PageNumber { get; set; }
    }
}
