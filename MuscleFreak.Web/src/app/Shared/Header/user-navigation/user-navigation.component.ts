import {Component, OnInit, Input} from '@angular/core';

import { AuthService } from '../../_services/auth.service';
import {ModalService} from "../../_services/modal.service";

@Component({
  templateUrl: 'user-navigation.component.html',
  styleUrls: ['user-navigation.component.scss'],
  selector: 'user-navigation'
})

export class UserNavigationComponent {

  @Input() loggedUsername: string;

  constructor(private _authService: AuthService, private _modalService: ModalService) {  }

  logout(){
    this._authService.logout();
  }

  openModal(id: string){
    this._modalService.open(id);
  }

  closeModal(id: string){
    this._modalService.close(id);
  }
}
