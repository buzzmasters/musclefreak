import { Component, OnInit } from '@angular/core';
import {ModalService} from "../../../_services/modal.service";
import {AuthService} from "../../../_services/auth.service";
import {User} from "../../../_models/user.model";

@Component({
  moduleId: module.id,
  selector: 'login-modal',
  templateUrl: 'login-modal.component.html'
  //styleUrls: ['login-modal.component.css']
})

export class LoginModalComponent {

  private username: string;
  private password: string;

  constructor(private _authService: AuthService, private _modalService: ModalService) {  }

  login() {
    let loginInfo = new User();
    loginInfo.username = this.username;
    loginInfo.password = this.password;

    this._authService.login(loginInfo).subscribe(() => {
      location.href = location.href;
    });
  }

  openModal(id: string) {
    this._modalService.open(id);
  }

  closeModal(id: string) {
    this._modalService.close(id);
  }
}
