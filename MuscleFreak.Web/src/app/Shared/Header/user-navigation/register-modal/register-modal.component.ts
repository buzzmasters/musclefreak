import { Component, OnInit } from '@angular/core';
import {ModalService} from "../../../_services/modal.service";

@Component({
  moduleId: module.id,
  selector: 'register-modal',
  templateUrl: 'register-modal.component.html'
  //styleUrls: ['register-modal.component.css']
})

export class RegisterModalComponent {

  constructor(private _modalService: ModalService) {  }

  openModal(id: string){
    this._modalService.open(id);
  }

  closeModal(id: string){
    this._modalService.close(id);
  }
}
