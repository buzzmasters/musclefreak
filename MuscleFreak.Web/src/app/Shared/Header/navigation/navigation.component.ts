import {Component, OnInit, Input} from '@angular/core';

@Component({
  templateUrl: 'navigation.component.html',
  styleUrls: ['navigation.component.scss'],
  selector: 'navigation'
})

export class NavigationComponent {

  @Input() loggedUsername: boolean = false;
}
