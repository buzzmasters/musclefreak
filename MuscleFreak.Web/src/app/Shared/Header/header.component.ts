import { Component, OnInit } from '@angular/core';
import {AuthService} from "../_services/auth.service";

@Component({
  templateUrl: 'header.component.html',
  styleUrls: ['header.component.scss'],
  selector: 'shared-header'
})

export class HeaderComponent {

  isLogged: string;

  constructor(private _authService: AuthService) {
    this.isLogged = _authService.isLogged();
  }


}
