export class Note {
  id: string;
  date: Date;
  bicepsL: number;
  bicepsR: number;
  chest: number;
  waist: number;
  hipL: number; // бедро
  hipR: number;
  shinL: number; // голень
  shinR: number;
  forearmL: number; // предплечье
  forearmR: number;
  weight: number;
}
