export class NoteSearchRequest {
  dateFrom: string;
  dateTo: string;
  username: string;
  pageSize: number;
  pageNumber: number;
}
