import { Injectable } from '@angular/core';
import {Http, Response, Headers} from '@angular/http';
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/do'

import { CookieService } from 'angular2-cookie/services/cookies.service';

import { User } from '../_models/user.model';
import { Token } from '../_models/token.model';

@Injectable()
export class AuthService {

  constructor(private http: Http, private cookieService: CookieService) {  }

  register(user: User): Observable<boolean> {
    return this.http.post('http://localhost/MuscleFreak.Auth.Service/api/register', user)
      .map((response: Response) => true);
  }

  login(user: User): Observable<Token> {

    let headers = new Headers();
    headers.append("Content-Type", "application/x-www-form-urlencoded");

    let token: Observable<Token> = this.http.post('http://localhost/MuscleFreak.Auth.Service/api/token',
      "userName=" + encodeURIComponent(user.username) +
      "&password=" + encodeURIComponent(user.password) +
      "&grant_type=password"/*{
      username: user.username,
      password: user.password,
      grant_type: "password"
      //client_id: "webUi"
    }*/, {
      headers: headers
    }).map((response: Response) => response.json())
      .do(tokenResponse => {

        this.cookieService.put("MuscleFreakAccessToken", tokenResponse.access_token, {
          expires: tokenResponse['.expires']
        });
        this.cookieService.put("MuscleFreakUsername", tokenResponse.userName, {
          expires: tokenResponse['.expires']
        });

        return tokenResponse;
      });

    return token;
  }

  logout(): void {
    const userName = this.cookieService.get("MuscleFreakAccessToken");
    const token = this.cookieService.get("MuscleFreakUsername");

    if (!userName || !token) {
      throw "You are not logged in to log out";
    }

    this.cookieService.remove("MuscleFreakAccessToken");
    this.cookieService.remove("MuscleFreakUsername");

    location.href = location.href;
  }

  isLogged(): string {
    const userName = this.cookieService.get("MuscleFreakUsername");
    const token = this.cookieService.get("MuscleFreakAccessToken");

    return userName && token ? userName : null;
  }

  getToken(): string {
    const token = this.cookieService.get("MuscleFreakAccessToken");
    return token;
  }
}
