import {Injectable} from "@angular/core";
import {Note} from "../_models/note.model";
import {Http, Headers, RequestOptions} from "@angular/http";
import { Observable } from 'rxjs';
import 'rxjs/add/operator/map'
import {AuthService} from "./auth.service";
import {NoteSearchRequest} from "../_models/note-search-request.model";

@Injectable()
export class NotesService {

  constructor(private _http: Http, private _authService: AuthService) { }

  addNote(note: Note): Observable<Note> {
    var headers = new Headers();
    headers.append("Authorization", `bearer ${this._authService.getToken()}`);

    return this._http.post("http://localhost/MuscleFreak.Notes.Service/api/notes", note, {
      headers: headers
    })
      .map(res => res.json());
  }

  getNotes(searchRequest: NoteSearchRequest): Observable<Array<Note>> {
    var headers = new Headers();
    headers.append("Authorization", `bearer ${this._authService.getToken()}`);
    headers.append("Content-Type", `application/json`);

    let params = [];
    for (let param in searchRequest) {
      params.push(`${param}=${searchRequest[param]}`);
    }

    return this._http.get(`http://localhost/MuscleFreak.Notes.Service/api/notes${params.length ? `?${params.join('&')}` : ''}`, {
      headers: headers
    })
      .map(res => res.json());
  }
}
