import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Routes, RouterModule } from '@angular/router';

import { HeaderComponent } from './Header/header.component';
import { NavigationComponent } from './Header/navigation/navigation.component';
import { UserNavigationComponent } from './Header/user-navigation/user-navigation.component';
import {ModalComponent} from "./_directives/modal.component";
import {LoginModalComponent} from "./Header/user-navigation/login-modal/login-modal.component";
import {RegisterModalComponent} from "./Header/user-navigation/register-modal/register-modal.component";
import {AuthService} from "./_services/auth.service";
import {ModalService} from "./_services/modal.service";
import {NotesService} from "./_services/notes.service";

@NgModule({
  declarations: [
    HeaderComponent,
    NavigationComponent,
    UserNavigationComponent,
    ModalComponent,
    LoginModalComponent,
    RegisterModalComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule
  ],
  exports: [
    HeaderComponent
  ],
  providers: [
    AuthService,
    ModalService,
    NotesService
  ]
})
export class SharedModule { }
