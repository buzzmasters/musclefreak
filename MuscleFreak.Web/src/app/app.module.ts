import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CookieService } from 'angular2-cookie/services/cookies.service';
import { DatepickerModule } from 'angular2-material-datepicker';

import { AppComponent } from './app.component';
import { HomeModule } from './Home/home.module';

/* --- Shared --- */
import { SharedModule } from './Shared/shared.module';

import { AppRoutingModule }        from './app-routing.module';
import { AuthService } from "./Shared/_services/auth.service";
import {ModalService} from "./Shared/_services/modal.service";
import {NotesModule} from "./Notes/notes.module";
import {StatisticsModule} from "./Statistics/statistics.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    AppRoutingModule,
    HttpModule,
    HomeModule,
    NotesModule,
    StatisticsModule,
    SharedModule
  ],
  providers: [CookieService],
  bootstrap: [AppComponent]
})
export class AppModule { }
