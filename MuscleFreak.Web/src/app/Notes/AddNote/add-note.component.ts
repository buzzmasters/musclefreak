import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {Note} from "../../Shared/_models/note.model";
import {NotesService} from "../../Shared/_services/notes.service";

@Component({
  moduleId: module.id,
  templateUrl: 'add-note.component.html',
  styleUrls: ['add-note.component.scss']
})

export class AddNoteComponent {
  @ViewChild('form') form:FormGroup;
  note: Note = new Note();
  currentHint: string;

  constructor(private _notesService: NotesService) { }

  showHint(hintName: string) {
    this.currentHint = hintName;
  }

  hideHint() {
    this.currentHint = null;
  }

  addNote() {
    if (this.form.valid) {
      this._notesService.addNote(this.note)
        .subscribe(()=> {
          location.href = "/stats"
        });
    }
  }
}
