import {Component, OnInit, ViewChild, ElementRef} from '@angular/core';
import {NotesService} from "../../Shared/_services/notes.service";
import {NoteSearchRequest} from "../../Shared/_models/note-search-request.model";
import {Note} from "../../Shared/_models/note.model";
import {Observable, Subscription} from "rxjs";

let Highcharts = require('highcharts');

@Component({
  moduleId: module.id,
  templateUrl: 'statistics.component.html',
  styleUrls: ['statistics.component.scss']
})

export class StatisticsComponent implements OnInit {

  dateFrom: Date;
  dateTo: Date;
  options: Object;

  request: Subscription;

  constructor(private _notesService: NotesService) {
    let dateTo = new Date();
    this.dateTo = dateTo;
    let dateFrom = new Date();
    dateFrom.setDate(dateFrom.getDate() - 7);
    this.dateFrom = dateFrom;
  }

  ngOnInit() {
    this.updateChart();
  }

  change(newDate) {
    if (this.request && !this.request.closed) {
      this.request.unsubscribe();
    }

    this.updateChart();
  }

  updateChart() {
    let searchRequest = new NoteSearchRequest();

    searchRequest.dateFrom = this.dateFrom.toUTCString();
    searchRequest.dateTo = this.dateTo.toUTCString();

    this.request = this._notesService.getNotes(searchRequest).subscribe(notes => {

      let measures: Object = {};

      notes.forEach(note => {
        for (let param in note) {
          if (param != 'id' && param != 'username' && param != 'date' && note[param] != null) {
            if (!measures.hasOwnProperty(param)) {
              measures[param] = {name: param, data: []}
            }
            let date = new Date(note.date);
            date.setHours(0,0,0,0);
            measures[param].data.push([date.getTime(), note[param]]);
          }
        }
      });

      let data = [];
      for (let measure in measures) {
        data.push({
          type: 'line',
          name: measures[measure].name,
          data: measures[measure].data
        })
      }

      this.options = {
        chart: {
          zoomType: 'x',
          width: 900
        },
        title: {
          text: 'Measures values over time'
        },
        subtitle: {
          text: document.ontouchstart === undefined ?
            'Click and drag in the plot area to zoom in' : 'Pinch the chart to zoom in'
        },
        xAxis: {
          type: 'datetime'
        },
        yAxis: {
          title: {
            text: 'Measure value, cm'
          }
        },
        legend: {
          enabled: true
        },

        series: data
      }
    }, null, () => console.log(123));
  }

  /*getDatesRange(startDate: Date, stopDate: Date) {
    var dateArray = new Array();
    var currentDate = startDate;
    while (currentDate <= stopDate) {
      dateArray.push( new Date (currentDate) );
      currentDate.setDate(currentDate.getDate() + 1);
    }
    return dateArray;
  }*/
}
