import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import {StatisticsComponent} from "./ShowStats/statistics.component";
import { DatepickerModule } from 'angular2-material-datepicker';
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import { ChartModule } from 'angular2-highcharts';

@NgModule({
  declarations: [
    StatisticsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    DatepickerModule,
    NoopAnimationsModule,
    ChartModule.forRoot(require('highcharts'))
  ],
  providers: []
})
export class StatisticsModule { }
