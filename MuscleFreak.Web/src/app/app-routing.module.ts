import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './Home/home.component';
import {AddNoteComponent} from "./Notes/AddNote/add-note.component";
import {StatisticsComponent} from "./Statistics/ShowStats/statistics.component";

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'stats', component: StatisticsComponent },
  { path: 'notes/add', component: AddNoteComponent },

  // otherwise redirect to home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
